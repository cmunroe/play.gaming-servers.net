# Let's Play

https://play.gaming-servers.net/


Let's play is a small php app for launching steam connect window. This is 
intended for linking to, so that you may quickly get your players in game.


Linking is meant to look nice, and pretty.

# Example links

``` https://play.gaming-servers.net/tf2-sjc.munroenet.com ```

This will launch you to: 72.13.84.54:27015. The port is assume if not stated.

``` https://play.gaming-servers.net/tf2-sjc.munroenet.com/27018/ ```

This will launch you to : 72.13.84.54:27018. The port is specificed in this option.

``` http://play.gaming-servers.net/ <server> / <port> ```

This is the basic syntax.

# Installation.

1. Install a web server of your choice.
2. Install php.
3. Make a subdomain for this site. i.e. (play)
4. Upload files to root of the site.
5. Profit!

# Donations

If you like my projects, and work then please donate! 

https://www.cameronmunroe.com/coffee/

# Credits

|  |   |
|--|---|
| Favicon | [Game Controller Icon](https://openclipart.org/detail/227918/video-game-controller-icon) |
| Background (bg/bg-min.jpeg) | [Lord of the Fallen](http://store.steampowered.com/app/265300/Lords_Of_The_Fallen/) |

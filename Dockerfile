#DockerFile
FROM php:8.3.3-apache

RUN a2enmod rewrite
RUN a2enmod remoteip
RUN echo "RemoteIPHeader X-Forwarded-For" >> /etc/apache2/apache2.conf
RUN sed -i 's/LogFormat "%h/LogFormat "%a/g' /etc/apache2/apache2.conf
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

EXPOSE 80
COPY web/ /var/www/html/

<?php
$ver = "0.2d";
// ---------------------------------\\
//		Load URL
// ---------------------------------\\

$failed = 0;
$valve = 0;
$red = null;

$myUrl = $_SERVER['REQUEST_URI'];

// Incase this is in a sub dir.
$myUrl = str_replace('/play/', '', $myUrl);
$myUrl = str_replace('/join/', '', $myUrl);

// Remove index from name.
$myUrl = str_replace('index.php/', '', $myUrl);
$myUrl = str_replace('index.php', '', $myUrl);

// In case someone tries to just put the ip:port into it. 
$myUrl = str_replace(':', '/', $myUrl); 

if(substr($myUrl, 0, 1) === "/"){
	$myUrl = substr($myUrl, 1);
}
	
$data = explode('/', $myUrl);

if(empty($data)){
	$failed = 1;
	$res = "<h2>Whoops! We don't know where you want to go!</h2>";
}

// ---------------------------------\\
//		Get Server
// ---------------------------------\\

if(!empty($data[0]) && filter_var($data[0], FILTER_VALIDATE_IP)){
	$ip = $data[0];
}

elseif(!empty($data[0]) && !filter_var($data[0], FILTER_VALIDATE_IP)){
	$ip = gethostbyname($data[0]);
}

else{
	$res = "<h2>Whoops! No server specified.</h2>";
	$failed = 1;
}

// ---------------------------------\\
//		Get Port
// ---------------------------------\\

if(!empty($data[1]) && is_numeric($data[1])){
	$port = $data[1];
}

elseif(!empty($data[1]) && !is_numeric($data[1])){
	$res = "<h2>Whoops! port is not a number.</h2>";
	$failed = 1;
}

else{
	$port = 27015;
}

if((strpos($_SERVER['HTTP_USER_AGENT'], 'Valve Client') !== false) && ($failed != 1)){
	
    $failed = 1;
    $res = "<h2>In console: connect $ip:$port</h2>";
}

if($failed != 1){

	$res = '<h2><a href="steam://connect/' . $ip . ':' . $port . '"/>Join ' . $ip . ':' . $port . ' now!</a></h2>';
	$red = '<meta http-equiv="refresh" content="1; url=steam://connect/' . $ip . ':' . $port . '/">';
}
?>
<html>
    <head>
        <title>Let's Play!</title>
		<link rel="preload" href="/img/bg-min.jpeg" as="image">
        <link rel="icon" type="image/x-icon" href="/img/favicon.ico">
        <link rel="apple-touch-icon-precomposed" href="/img/video-game-controller-icon-152-227918.png">
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/img/video-game-controller-icon-152-227918.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/img/video-game-controller-icon-144-227918.png">
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/img/video-game-controller-icon-120-227918.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/img/video-game-controller-icon-114-227918.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/img/video-game-controller-icon-72-227918.png">
        <link rel="apple-touch-icon-precomposed" href="/img/video-game-controller-icon-57-227918.png">
        <link rel="icon" href="/img/video-game-controller-icon-32-227918.png" sizes="32x32">
        <meta name="msapplication-TileColor" content="#ffffff" />
        <meta name="msapplication-TileImage" content="/img/video-game-controller-icon-144-227918.png">

        
        <style>
        
            body {
				background-image: url("/img/bg-min.jpeg");
				background-repeat: no-repeat;
				background-attachment: fixed;
				background-size: cover;
				background-color: black;
				font-family: georgia;
			}
          	h1 {
				font-size: 800%;
				color: white;
				text-align: center;
				padding-top: 250px;
				text-shadow: -2px 0 black, 0 2px black, 2px 0 black, 0 -2px black;
			}
            .statusbox {
				margin: 0 auto;
				width:600px;
				height:80px;
				background-color: white;
				border-radius: 10px;
				border: 2px solid black;
          		
			}
			.status {
				padding: 10px;
				text-align: center;
				color: black;
				
			}
			
			.footer {
				width: 100%;
				font-size: 69%;
				text-align: center;
				height:30px;
				position: fixed;
				bottom: 0;
				left: 0;
				right: 0;
				color: black;
				background-color: white;
				
			}
			a {
				color: black;
			}
			
        </style>
		
		<?php echo $red; ?>
		
        
    </head>
    
    <body>
        <h1>Let's Play!</h1>
		<div class="statusbox">
			<div class="status">
				<?php echo $res; ?>
				
			</div>
		</div>
		<div class="footer">
			<p> Ver. <?php echo $ver; ?>, by Munzy! Get your copy on <a href="https://github.com/Munzy/lets-play">Github</a>. Please <a href="https://www.cameronmunroe.com/coffee/">donate</a> if you like my work. </p>
		</div>
    </body>
</html>